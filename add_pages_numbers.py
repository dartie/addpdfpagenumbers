import sys, os, shutil
from argparse import ArgumentParser, RawTextHelpFormatter, SUPPRESS
import reportlab
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.lib.colors import HexColor, gray
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics

from PyPDF2 import PdfFileWriter, PdfFileReader

TTFSearchPath = (
            'c:/winnt/fonts',
            'c:/windows/fonts',
            '/usr/lib/X11/fonts/TrueType/',
            '/usr/share/fonts/truetype',
            '/usr/share/fonts',             #Linux, Fedora
            '/usr/share/fonts/dejavu',      #Linux, Fedora
            '%(REPORTLAB_DIR)s/fonts',      #special
            '%(REPORTLAB_DIR)s/../fonts',   #special
            '%(REPORTLAB_DIR)s/../../fonts',#special
            '%(CWD)s/fonts',                #special
            '~/fonts',
            '~/.fonts',
            '%(XDG_DATA_HOME)s/fonts',
            '~/.local/share/fonts',
            #mac os X - from
            #http://developer.apple.com/technotes/tn/tn2024.html
            '~/Library/Fonts',
            '/Library/Fonts',
            '/Network/Library/Fonts',
            '/System/Library/Fonts',
            )

pdfmetrics.registerFont(TTFont('Vera', 'Vera.ttf'))
pdfmetrics.registerFont(TTFont('VeraBd', 'VeraBd.ttf'))
pdfmetrics.registerFont(TTFont('VeraIt', 'VeraIt.ttf'))
pdfmetrics.registerFont(TTFont('VeraBI', 'VeraBI.ttf'))


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Add Page numbers to PDF files

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=SUPPRESS)
                        # help='Increase output verbosity. Output files don\'t overwrite original ones'

    parser.add_argument("-x", dest="x", type=int, default=400,
                        help="Page Number X position")

    parser.add_argument("-y", dest="y", type=int, default=290,
                        help="Page Number Y position")

    parser.add_argument("-f", "--font", dest="font_family", default="Vera",
                        help="Page Number font size")

    parser.add_argument("-s", "--font-size", dest="font_size", default=11, type=int,
                        help="Page Number font size")

    parser.add_argument("-c", "--font-color", dest="font_color", default=None,
                        help="Page Number font color - Default: Gray")

    parser.add_argument("--skip-first-page", dest="skip_first_page", action='store_true', default=False,
                        help="Don't apply a number to the first page")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    parser.add_argument("pdf_file",
                        help="PDF file to add page numbers")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def createPagePdf(num, tmp, args):
    offset = 0 if args.skip_first_page else 1

    if args.font_color:
        args.font_color = HexColor(args.font_color)
    else:
        args.font_color = gray

    c = canvas.Canvas(tmp)

    if args.skip_first_page:
        c.showPage()

    for i in range(1,num + offset):
        c.setFillColor(args.font_color)
        try:
            c.setFont(args.font_family, args.font_size)
        except:
            print('Font Family not found - using Verdana')
            c.setFont("Vera", args.font_size)
        c.drawString((args.x//2)*mm, (args.y)*mm, str(i))
        c.showPage()
    c.save()
    return
    with open(tmp, 'rb') as f:
        pdf = PdfFileReader(f)
        layer = pdf.getPage(0)
    return layer


def main(args=None):
    if args is None:
        args = check_args()

    path = args.pdf_file
    base = os.path.basename(path)
    
    tmp = "__tmp.pdf"
    # batch = 10
    batch = 0
    output = PdfFileWriter()
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f,strict=False)
        n = pdf.getNumPages()
        if batch == 0:
            batch = -n
        createPagePdf(n,tmp, args)
        
        # create output directory if not exists
        # if not os.path.isdir('pdfWithNumbers/'):
        #     os.mkdir('pdfWithNumbers/')
        
        with open(tmp, 'rb') as ftmp:
            numberPdf = PdfFileReader(ftmp)
            for p in range(n):
                if not p%batch and p:
                    #newpath = path.replace(base, 'pdfWithNumbers/'+ base[:-4] + '_page_%d'%(p//batch) + path[-4:])
                    newpath = os.path.join(os.path.dirname(path), os.path.splitext(base)[0] + '-pages' + os.path.splitext(base)[1])
                    with open(newpath, 'wb') as f:
                        output.write(f)
                    output = PdfFileWriter()
#                sys.stdout.write('\rpage: %d of %d'%(p, n))
                print('page: %d of %d'%(p, n))
                page = pdf.getPage(p)
                numberLayer = numberPdf.getPage(p)
                
                page.mergePage(numberLayer)
                output.addPage(page)
            if output.getNumPages():
                #newpath = path.replace(base, 'pdfWithNumbers/' + base[:-4] + '_page_%d'%(p//batch + 1)  + path[-4:])
                newpath = os.path.join(os.path.dirname(path), os.path.splitext(base)[0] + '-pages'+ os.path.splitext(base)[1])
                with open(newpath, 'wb') as f:
                    output.write(f)
    #input_bkp = os.path.join(os.path.dirname(path), os.path.splitext(base)[0] + '-bkp'+ os.path.splitext(base)[1])
    shutil.copy2(newpath, path)
    #os.remove(tmp)


if __name__ == "__main__":
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
